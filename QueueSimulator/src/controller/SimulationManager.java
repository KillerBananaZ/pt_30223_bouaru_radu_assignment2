package controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import file.Reader;
import file.Writer;
import model.Task;
import model.TaskComparator;

public class SimulationManager implements Runnable {

	private int timeLimit;
	private int maxProcessingTime;
	private int minProcessingTime;
	private int maxArrivalTime;
	private int minArrivalTime;
	private int numberOfServers;
	private int numberOfClients;
	public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
	public static AtomicInteger sum = new AtomicInteger(0);
	
	private Scheduler scheduler;

	private List<Task> generatedTasks;
	
	protected Writer writer;
	protected Reader reader;

	public SimulationManager(String inputFileName, String outputFileName) throws IOException {
		generatedTasks = new ArrayList<Task>();
		
		reader = new Reader(inputFileName);
		this.numberOfServers = reader.getNumberOfQueues();
		this.numberOfClients = reader.getNumberOfClients();
		this.timeLimit = reader.getSimulationInterval();
		this.minArrivalTime = reader.getMinArrivalTime();
		this.maxArrivalTime = reader.getMaxArrivalTime();
		this.minProcessingTime = reader.getMinServiceTime();
		this.maxProcessingTime = reader.getMaxServiceTime();
		
		scheduler = new Scheduler(numberOfServers, numberOfClients, timeLimit, outputFileName);
		scheduler.changeStrategy(selectionPolicy);
		generateNRandomTasks();
		writer = new Writer(outputFileName);		
	}

	private void generateNRandomTasks() {
		Random r = new Random();
		for (int i = 1; i <= numberOfClients; i++) {
			Task newTask = new Task(0, 0, 0);
			newTask.setID(i);
			int tService = r.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;
			newTask.settService(tService);
			int tArrival = r.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
			newTask.settArrival(tArrival);
			this.generatedTasks.add(newTask);
		}
		generatedTasks.sort(new TaskComparator());
	}

	@Override
	public void run() {
		int currentTime = 0;
		while (currentTime < timeLimit) {
			try {
				
				Thread.sleep(500);
			} catch (Exception ex) {
				System.out.println(ex.toString());
			}
			writer.write("\n\nTime: " + currentTime);
			List<Task> deletionList = new ArrayList<Task>();
			for (Task t : generatedTasks) {
				if (t.gettArrival() == currentTime) {
					sum.getAndAdd(t.gettService());
					scheduler.dispatchTask(t);
					deletionList.add(t);
				}
			}
			generatedTasks.removeAll(deletionList);			
			writer.write("\nWaiting clients: ");
			for (Task t : generatedTasks)
				writer.write("(" + t.getID() + "," + t.gettArrival() + "," + t.gettService() + ") ");

			currentTime++;
			try {
				Thread.sleep(500);
				
			} catch (Exception ex) {
				System.out.println(ex.toString());
			}
		}
		try {
			Thread.sleep(1000);
			writer.write("\n\nAverage waiting time = " + (sum.intValue() * 1.0) / numberOfClients + " seconds.");
			System.out.println("Execution finished, check the output file for the results!");
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
	}

}
