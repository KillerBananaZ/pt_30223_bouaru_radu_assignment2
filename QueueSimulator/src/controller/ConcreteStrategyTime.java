package controller;
import java.util.List;

import model.Server;
import model.Task;

public class ConcreteStrategyTime implements Strategy{

	@Override
	public void addTask(List<Server> servers, Task t) {
	
		int minPeriod = 32000;
		
		for(Server s: servers)
		{			
			if(s.getWaitingPeriod().intValue() < minPeriod)
			{
				minPeriod = s.getWaitingPeriod().intValue();
			}
		}
		
		for(Server s: servers)
		{
			if(s.getWaitingPeriod().intValue() == minPeriod)
			{
				if(s.getTasks().isEmpty())
				{
					t.settService(t.gettService() + 1);
				}
				s.addTask(t);
				break;
			}
		}

	}

}
