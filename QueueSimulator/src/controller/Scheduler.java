package controller;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import model.Server;
import model.Task;

public class Scheduler {
	private List<Server> servers;

	private Strategy strategy;

	public Scheduler(int maxNoServers, int maxTasksPerServer, int timeLimit, String outputFileName) throws IOException {
		servers = Collections.synchronizedList(new LinkedList<Server>());
		
		for (int i = 0; i < maxNoServers; i++) {
			Server newServer = new Server(maxTasksPerServer, i+1, timeLimit, outputFileName);
			Thread newThread = new Thread(newServer);
			servers.add(newServer);
			newThread.start();
		}
	}

	public void changeStrategy(SelectionPolicy policy) {
		if (policy == SelectionPolicy.SHORTEST_TIME) {
			strategy = new ConcreteStrategyTime();
		}
	}

	public void dispatchTask(Task t) {
		strategy.addTask(servers, t);
	}

	public List<Server> getServers() {
		return servers;
	}
}
