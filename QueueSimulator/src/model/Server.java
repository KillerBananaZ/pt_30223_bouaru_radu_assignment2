package model;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import file.Writer;

public class Server implements Runnable {
	private BlockingQueue<Task> tasks;
	private AtomicInteger waitingPeriod;
	private int currentTime = 0;
	private int timeLimit;
	private int serverIndex;
	
	protected Writer writer;
	
	public Server(int maxTasksPerServer, int serverIndex, int timeLimit, String outputFileName) throws IOException {
		this.tasks = new LinkedBlockingQueue<Task>();
		this.waitingPeriod = new AtomicInteger(0);
		this.serverIndex = serverIndex;
		this.timeLimit = timeLimit;
		
		writer = new Writer(outputFileName);
	}

	public void addTask(Task newTask) {
		this.tasks.add(newTask);
		this.waitingPeriod.getAndAdd(newTask.gettService());
	}

	@Override
	public void run() {
		
		String outputString = "";
		
		while (currentTime < timeLimit) {
			try {
				Thread.sleep(999);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			outputString = "\nQueue " + this.serverIndex + ": closed";
			
			if (!this.tasks.isEmpty()) {
				if (this.tasks.peek().gettService() <= 1) {
					this.tasks.remove();
				}
				if (!tasks.isEmpty()) {
					tasks.peek().settService(tasks.peek().gettService() - 1);
				} else {
					this.waitingPeriod.getAndSet(0);
				}

				outputString = "\nQueue " + this.serverIndex + "-> ";
				
				for (Task t : this.tasks)
					outputString += "(" + t.getID() + "," + t.gettArrival() + "," + t.gettService() + ") ";
				
				if(tasks.isEmpty())
				{
					outputString = "\nQueue " + this.serverIndex + ": closed";
					this.waitingPeriod.getAndSet(0);
				}					
				writer.write(outputString);					
				waitingPeriod.getAndDecrement();
			} else {
				writer.write(outputString);	
				this.waitingPeriod.getAndSet(0);
			}
			
			currentTime++;
		}
	}

	public BlockingQueue<Task> getTasks() {
		return this.tasks;
	}

	public AtomicInteger getWaitingPeriod() {
		return this.waitingPeriod;
	}

	public int getServerIndex() {
		return this.serverIndex;
	}
}
