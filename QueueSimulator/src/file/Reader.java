package file;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Reader {
	
	private ArrayList<Integer> numericValues = new ArrayList<Integer>();

	public Reader(String fileName)
	{
		try {
			Scanner scanner = new Scanner(new File(fileName));
			int numberOfLines = 0;
			while(scanner.hasNextLine())
			{
				numberOfLines++;
				scanner.nextLine();
			}
			if(numberOfLines != 5)
			{
				System.out.println("Wrong input file format!");
				System.exit(1);
			}
			else
			{
				scanner = new Scanner(new File(fileName));
				
				while(scanner.hasNextLine())
				{					 
					String[] nextLine = scanner.nextLine().split(",");
					for(String s: nextLine)
						numericValues.add(Integer.parseInt(s.trim()));
				}
			}
			scanner.close();
		}
		catch(FileNotFoundException ex)
		{
			System.out.println("Wrong input file!");
			System.exit(2);
		}
	}
	public int getNumberOfClients() {
		return this.numericValues.get(0);
	}
	public int getNumberOfQueues() {
		return this.numericValues.get(1);
	}
	public int getSimulationInterval() {
		return this.numericValues.get(2);
	}
	public int getMinArrivalTime() {
		return this.numericValues.get(3);
	}
	public int getMaxArrivalTime() {
		return this.numericValues.get(4);
	}
	public int getMinServiceTime() {
		return this.numericValues.get(5);
	}
	public int getMaxServiceTime() {
		return this.numericValues.get(6);
	}

}
