package file;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Writer {

	protected String fileName;
	private static boolean appendFile = false;
	
	public Writer(String fileName) {
		this.fileName = fileName;
	}

	public void write(String toBeWritten) {
		FileWriter writer;
		try {
				
				writer = new FileWriter(this.fileName, appendFile);
				writer.write(toBeWritten);		
				appendFile = true;
				writer.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
