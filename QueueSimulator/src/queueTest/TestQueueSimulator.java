package queueTest;

import java.io.IOException;

import controller.SimulationManager;

public class TestQueueSimulator {

	public static void main(String[] args) throws IOException {
		SimulationManager queueSimulator = new SimulationManager(args[0], args[1]);
		Thread queueSimulatorThread = new Thread(queueSimulator);
		System.out.println("Simulation has begun");
		queueSimulatorThread.run();
	}

}
